import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.felixcool98.simplegdx.Game;

public class TestMain extends Game {
	public static void main(String[] args) {
		new Lwjgl3Application(new TestMain(), new Lwjgl3ApplicationConfiguration());
	}
	
	
	public TestMain() {
		super(20);
	}

	
	@Override
	public void update() {
		
	}

	@Override
	public void draw() {
		drawImage("Pen.png", 0, 0, 5, 5);
		drawLine(0, 0, -1, -1, Color.GREEN, 1);
	}
}
