package com.felixcool98.simplegdxlauncher.desktop;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

public class Launch {
	private Launch() {}
	
	
	public static void launch(int width, int height, ApplicationListener listener) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		
		config.setWindowSizeLimits(width, height, width, height);
		
		new Lwjgl3Application(listener, config);
	}
}
