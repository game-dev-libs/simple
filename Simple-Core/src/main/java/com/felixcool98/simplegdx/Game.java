package com.felixcool98.simplegdx;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
public abstract class Game extends ApplicationAdapter {
	private Internals internals;
	
	
	public Game(float viewWidth) {
		internals = new Internals(viewWidth);
	}
	
	
	public abstract void update();
	public abstract void draw();
	
	
	@Override
	public void create() {
		internals.create();
	}
	
	
	@Override
	public final void render() {
		update();
		
		internals.getCamera().update();
		internals.getBatch().setProjectionMatrix(internals.getCamera().combined);
		internals.getShapes().setProjectionMatrix(internals.getCamera().combined);
		
		draw();
		
		if(internals.getBatch().isDrawing())
			internals.getBatch().end();
		if(internals.getShapes().isDrawing())
			internals.getShapes().end();
	}
	
	
	private void prepareBatch() {
		if(internals.getShapes().isDrawing())
			internals.getShapes().end();
		if(!internals.getBatch().isDrawing())
			internals.getBatch().begin();
	}
	
	public void drawImage(String path, float x, float y, float width, float height) {
		prepareBatch();
		
		FileHandle handle = Gdx.files.local(path);
		
		if(!internals.getAssets().contains(handle.name()))
			internals.getAssets().load(handle.name(), Texture.class);
		if(!internals.getAssets().isLoaded(handle.name()))
			internals.getAssets().finishLoading();
		
		internals.getBatch().draw(internals.getAssets().get(path, Texture.class), x, y, width, height);
	}
	
	
	private void prepareShapes() {
		if(internals.getBatch().isDrawing())
			internals.getBatch().end();
		if(!internals.getShapes().isDrawing())
			internals.getShapes().begin();
	}
	
	public void drawLine(float x, float y, float x2, float y2, Color color, float width) {
		prepareShapes();
		
		internals.getShapes().set(ShapeType.Filled);
		internals.getShapes().rectLine(x, y, x2, y2, width, color, color);
	}
	
	
	public void cameraCenter(float x, float y) {
		internals.getCamera().position.set(x, y, 0);
	}
	public void moveCamera(float x, float y) {
		internals.getCamera().position.add(x, y, 0);
	}
	
	
	public Internals getInternals() {
		return internals;
	}
	
	
	@Override
	public void dispose() {
		internals.getAssets().dispose();
	}
}
