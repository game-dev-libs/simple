package com.felixcool98.simplegdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

class Internals {
	private Batch batch;
	private ShapeRenderer shapes;
	private Camera camera;
	
	
	private AssetManager assets;
	
	private float viewWidth;
	
	
	Internals(float viewWidth) {
		this.viewWidth = viewWidth;
	}
	
	
	public void create() {
		camera  = new OrthographicCamera(viewWidth, (float)Gdx.graphics.getHeight()/(float)Gdx.graphics.getWidth()*viewWidth);
		
		assets = new AssetManager();
		
		batch = new SpriteBatch();
		
		shapes = new ShapeRenderer();
		shapes.setAutoShapeType(true);
	}
	
	
	public Batch getBatch() {
		return batch;
	}
	public ShapeRenderer getShapes() {
		return shapes;
	}
	public Camera getCamera() {
		return camera;
	}
	public AssetManager getAssets() {
		return assets;
	}
}
